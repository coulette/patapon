
#define _DEG THE_DEG
#define _RAF THE_RAF
#define _M   THE_M

#define _NP (_DEG + 1)

#define _NPG_ROW ((_DEG + 1) * _RAF)

#define _NPG_FACE (_NPG_ROW * _NPG_ROW)

#define _NPG_MACROCELL (_NPG_ROW * _NPG_FACE)

// number of nodes in each hexaedron cell
#define _NHEXA 8

__constant int npg = _NP * _NP * _NP * _RAF * _RAF * _RAF;


//! Gauss LObatto Points (GLOP) up to order 4
__constant float gauss_lob_point[] = {
  0.5,
  0,
  1,
  0,
  0.5,
  1,
  0,
  0.276393202250021030359082633127,
  0.723606797749978969640917366873,
  1,
  0,
  0.172673164646011428100853771877,
  0.5,
  0.827326835353988571899146228123,
  1
};

//! GLOP weights up to order 4
__constant float gauss_lob_weight[] = {
  1,
  0.5,
  0.5,
  0.166666666666666666666666666667,
  0.666666666666666666666666666668,
  0.166666666666666666666666666667,
  0.0833333333333333333333333333333,
  0.416666666666666666666666666666,
  0.416666666666666666666666666666,
  0.0833333333333333333333333333333,
  0.05,
  0.272222222222222222222222222223,
  0.355555555555555555555555555556,
  0.272222222222222222222222222219,
  0.05
};

//! indirection for finding the GLOP
//! data for a given degree in the previous arrays
__constant int gauss_lob_offset[] = {0, 1, 3, 6, 10};

__constant float gauss_lob_dpsi[] = {
  0.0,
  -1.,
  -1.,
  1.,
  1.,
  -3.,
  -1.,
  1.,
  4.,
  0.,
  -4.,
  -1.,
  1.,
  3.,
  -6,
  -1.61803398874989484820458683436,
  .618033988749894848204586834362,
  -1,
  8.09016994374947424102293417177,
  0,
  -2.23606797749978969640917366872,
  3.09016994374947424102293417184,
  -3.09016994374947424102293417182,
  2.23606797749978969640917366872,
  0,
  -8.09016994374947424102293417177,
  1,
  -.618033988749894848204586834362,
  1.61803398874989484820458683436,
  6,
  -10,
  -2.48198050606196571569743868436,
  .75,
  -.518019493938034284302561315632,
  1,
  13.5130049774484800076860550594,
  0,
  -2.67316915539090667050969419631,
  1.52752523165194666886268239794,
  -2.82032835588485332564727827404,
  -5.33333333333333333333333333336,
  3.49148624377587810025755976667,
  0,
 -3.49148624377587810025755976662,
  5.33333333333333333333333333336,
  2.82032835588485332564727827399,
  -1.52752523165194666886268239791,
  2.67316915539090667050969419635,
  0,
  -13.5130049774484800076860550594,
  -1,
  .518019493938034284302561315631,
  -.75,
  2.48198050606196571569743868437,
  10
};

//! indirection for finding the GLOP
//! data for a given degree in the previous arrays
__constant int gauss_lob_dpsi_offset[] = {0, 1, 5, 14, 30};

float dlag(int deg, int ib, int ipg) {
  return gauss_lob_dpsi[gauss_lob_dpsi_offset[deg] + ib * (deg + 1) + ipg];
}


//! \brief 1d GLOP weights for a given degree
//! \param[in] deg degree
//! \param[in] i glop index
//! \returns the glop weight
float wglop(int deg, int i) {
  return gauss_lob_weight[gauss_lob_offset[deg] + i];
}

float glop(int deg, int i){
  return gauss_lob_point[gauss_lob_offset[deg] + i];
}


int Varindex(int ipg, int iv) {
  return ipg + npg * iv;
}


__constant float vit[3]={1, 0, 0};

void NumFlux(float *wL, float *wR, float *vnorm, float *flux)
{
  float vn = vit[0] * vnorm[0] + vit[1] * vnorm[1] + vit[2] * vnorm[2];
  float vnp = vn > 0.0 ? vn : 0.0;
  float vnm = vn - vnp;
  flux[0] = vnp * wL[0] + vnm * wR[0];
  vn = -vn;
  vnp = vn > 0.0 ? vn : 0.0;
  vnm = vn - vnp;
  flux[1] = vnp * wL[1] + vnm * wR[1];
}

void ImposedData(const float *x, const float t, float* w)
{
  float vx = vit[0] * x[0] + vit[1] * x[1] + vit[2] * x[2];
  float xx = vx - t;
  float pi = 3.1415926535897932384626433832795028841971694;
  //w[0] = xx * xx;
  w[0] = sin(2*pi*xx);
  w[0]=xx;
  xx = vx + t;
  //w[1] = xx * xx;
  w[1] = sin(2*pi*xx);
  w[1]=xx;
}


void BoundaryFlux(float *x, float t,
			    float *wL, float *vnorm,
			    float *flux)
{
  float wR[2];
  ImposedData(x, t, wR);
  NumFlux(wL, wR, vnorm, flux);
}

void get_tau(float x, float y, float z,
	     __constant float *p, float xphy[]){

  float phi[_NHEXA];
  
  float t1 = -1 + z;
  float t2 = -1 + y;
  float t4 = -1 + x;
  float t8 = x * y;
  phi[0] = -t1 * t2 * t4;
  phi[1] = x * t1 * t2;
  phi[2] = -t8 * t1;
  phi[3] = y * t1 * t4;
  phi[4] = z * t2 * t4;
  phi[5] = -x * z * t2;
  phi[6] = t8 * z;
  phi[7] = -y * z * t4;

  xphy[0] = 0;
  xphy[1] = 0;
  xphy[2] = 0;
 
 for(int ii = 0; ii < _NHEXA; ii++){
   xphy[0] += p[ii * 3 + 0] * phi[ii]; 
   xphy[1] += p[ii * 3 + 1] * phi[ii]; 
   xphy[2] += p[ii * 3 + 2] * phi[ii];
 }
 
}

void get_dtau(float x, float y, float z,
	      __constant float *p, float dtau[][3]){
float t1 = -1 + z;
float t2 = -1 + y;
float t3 = t1 * t2;
float t6 = y * t1;
float t9 = z * t2;
float t12 = y * z;
float t16 = -1 + x;
float t17 = t1 * t16;
float t19 = x * t1;
float t23 = z * t16;
float t25 = x * z;
float t30 = t2 * t16;
float t32 = x * t2;
float t34 = x * y;
float t36 = y * t16;
 
 dtau[0][0] = -t3 * p[0] + t3 * p[3] - t6 * p[6] + t6 * p[9] +
   t9 * p[12] - t9 * p[15] + t12 * p[18] - t12 * p[21];
 dtau[0][1] = -t17 * p[0] + t19 * p[3] - t19 * p[6] + t17 * p[9]
   + t23 * p[12] - t25 * p[15] + t25 * p[18] - t23 * p[21];
 dtau[0][2] = -t30 * p[0] + t32 * p[3] - t34 * p[6] + t36 * p[9]
   + t30 * p[12] - t32 * p[15] + t34 * p[18] - t36 * p[21];
 dtau[1][0] = -t3 * p[1] + t3 * p[4] - t6 * p[7] + t6 * p[10]
   + t9 * p[13] - t9 * p[16] + t12 * p[19] - t12 * p[22];
 dtau[1][1] = -t17 * p[1] + t19 * p[4] - t19 * p[7] + t17 * p[10]
   + t23 * p[13] - t25 * p[16] + t25 * p[19] - t23 * p[22];
 dtau[1][2] = -t30 * p[1] + t32 * p[4] - t34 * p[7] + t36 * p[10]
   + t30 * p[13] - t32 * p[16] + t34 * p[19] - t36 * p[22];
 dtau[2][0] = -t3 * p[2] + t3 * p[5] - t6 * p[8] + t6 * p[11]
   + t9 * p[14] - t9 * p[17] + t12 * p[20] - t12 * p[23];
 dtau[2][1] = -t17 * p[2] + t19 * p[5] - t19 * p[8] + t17 * p[11]
   + t23 * p[14] - t25 * p[17] + t25 * p[20] - t23 * p[23];
 dtau[2][2] = -t30 * p[2] + t32 * p[5] - t34 * p[8] + t36 * p[11]
   + t30 * p[14] - t32 * p[17] + t34 * p[20] - t36 * p[23];


/* dtau[0][0] = -(-1 + z) * (-1 + y) * p[0] + (-1 + z) * (-1 + y) * p[3] - y * (-1 + z) * p[6] + y * (-1 + z) * p[9] + z * (-1 + y) * p[12] - z * (-1 + y) * p[15] + y * z * p[18] - y * z * p[21]; */
/* dtau[0][1] = -(-1 + z) * (-1 + x) * p[0] + x * (-1 + z) * p[3] - x * (-1 + z) * p[6] + (-1 + z) * (-1 + x) * p[9] + z * (-1 + x) * p[12] - x * z * p[15] + x * z * p[18] - z * (-1 + x) * p[21]; */
/* dtau[0][2] = -(-1 + y) * (-1 + x) * p[0] + x * (-1 + y) * p[3] - x * y * p[6] + y * (-1 + x) * p[9] + (-1 + y) * (-1 + x) * p[12] - x * (-1 + y) * p[15] + x * y * p[18] - y * (-1 + x) * p[21]; */
/* dtau[1][0] = -(-1 + z) * (-1 + y) * p[1] + (-1 + z) * (-1 + y) * p[4] - y * (-1 + z) * p[7] + y * (-1 + z) * p[10] + z * (-1 + y) * p[13] - z * (-1 + y) * p[16] + y * z * p[19] - y * z * p[22]; */
/* dtau[1][1] = -(-1 + z) * (-1 + x) * p[1] + x * (-1 + z) * p[4] - x * (-1 + z) * p[7] + (-1 + z) * (-1 + x) * p[10] + z * (-1 + x) * p[13] - x * z * p[16] + x * z * p[19] - z * (-1 + x) * p[22]; */
/* dtau[1][2] = -(-1 + y) * (-1 + x) * p[1] + x * (-1 + y) * p[4] - x * y * p[7] + y * (-1 + x) * p[10] + (-1 + y) * (-1 + x) * p[13] - x * (-1 + y) * p[16] + x * y * p[19] - y * (-1 + x) * p[22]; */
/* dtau[2][0] = -(-1 + z) * (-1 + y) * p[2] + (-1 + z) * (-1 + y) * p[5] - y * (-1 + z) * p[8] + y * (-1 + z) * p[11] + z * (-1 + y) * p[14] - z * (-1 + y) * p[17] + y * z * p[20] - y * z * p[23]; */
/* dtau[2][1] = -(-1 + z) * (-1 + x) * p[2] + x * (-1 + z) * p[5] - x * (-1 + z) * p[8] + (-1 + z) * (-1 + x) * p[11] + z * (-1 + x) * p[14] - x * z * p[17] + x * z * p[20] - z * (-1 + x) * p[23]; */
/* dtau[2][2] = -(-1 + y) * (-1 + x) * p[2] + x * (-1 + y) * p[5] - x * y * p[8] + y * (-1 + x) * p[11] + (-1 + y) * (-1 + x) * p[14] - x * (-1 + y) * p[17] + x * y * p[20] - y * (-1 + x) * p[23]; */
/* dtau[0][0] = 1; */
/* dtau[0][1] = 0; */
/* dtau[0][2] = 0; */
/* dtau[1][0] = 0; */
/* dtau[1][1] = 1; */
/* dtau[1][2] = 0; */
/* dtau[2][0] = 0; */
/* dtau[2][1] = 0; */
/* dtau[2][2] = 1; */

 
}

__kernel
void get_nodes(int ie,                    // 1: macrocel index
		   __constant float *physnodes, // 2: macrocell nodes
		   __global float *node       // 3: node coordinates
	      )
{
  __constant float *physnode = physnodes + ie * 3 * _NHEXA;
 
  int ix = get_global_id(0);
  int iy = get_global_id(1); 
  int iz = get_global_id(2);
 
  //int woffset = ie * _M * _NPG_MACROCELL;

  // subcell id
  int icL[3];
  icL[0] = ix / _NP;
  icL[1] = iy / _NP;
  icL[2] = iz / _NP;

  int pL[3];
  pL[0] = ix % _NP;
  pL[1] = iy % _NP;
  pL[2] = iz % _NP;

  
  // ref coordinates
  float hx = 1. / (float) _RAF;
  float hy = 1. / (float) _RAF;
  float hz = 1. / (float) _RAF;

  int offset[3] = {gauss_lob_offset[_DEG] + pL[0],
		   gauss_lob_offset[_DEG] + pL[1],
		   gauss_lob_offset[_DEG] + pL[2]};

  float x = hx * (icL[0] + gauss_lob_point[offset[0]]);
  float y = hy * (icL[1] + gauss_lob_point[offset[1]]);
  float z = hz * (icL[2] + gauss_lob_point[offset[2]]);

  int ipgL = pL[0] + _NP * icL[0] + _NPG_ROW * (iy + _NPG_ROW * iz);


  float xphy[3];
  get_tau(x, y, z, physnode, xphy);

  node[ipgL + 0 * _NPG_MACROCELL] = xphy[0];
  node[ipgL + 1 * _NPG_MACROCELL] = xphy[1];
  node[ipgL + 2 * _NPG_MACROCELL] = xphy[2];

  
  /* printf("ipgL=%d ijk=%d %d %d xyz=%f %f %f xphy=%f %f %f\n", */
  /* 	 ipgL,ix,iy,iz, */
  /* 	 x,y,z,xphy[0],xphy[1],xphy[2]); */
 

}

  
__kernel
void Init(int ie,                    // 1: macrocel index
	  __constant float *physnodes, // 2: macrocell nodes
	  __global float *wn        // 3: field values
	      )
{
  __constant float *physnode = physnodes + ie * 3 * _NHEXA;

  int ix = get_global_id(0);
  int iy = get_global_id(1); 
  int iz = get_global_id(2);
 
  int woffset = ie * _M * _NPG_MACROCELL;

  // subcell id
  int icL[3];
  icL[0] = ix / _NP;
  icL[1] = iy / _NP;
  icL[2] = iz / _NP;

  int pL[3];
  pL[0] = ix % _NP;
  pL[1] = iy % _NP;
  pL[2] = iz % _NP;

  
  // ref coordinates
  float hx = 1. / _RAF;
  float hy = 1. / _RAF;
  float hz = 1. / _RAF;

  int offset[3] = {gauss_lob_offset[_DEG] + pL[0],
		   gauss_lob_offset[_DEG] + pL[1],
		   gauss_lob_offset[_DEG] + pL[2]};

  float x = hx * (icL[0] + gauss_lob_point[offset[0]]);
  float y = hy * (icL[1] + gauss_lob_point[offset[1]]);
  float z = hz * (icL[2] + gauss_lob_point[offset[2]]);

  int ipgL = pL[0] + _NP * icL[0] + _NPG_ROW * (iy + _NPG_ROW * iz);

  float xphy[3];
  get_tau(x, y, z, physnode, xphy);

  float wL[_M];
  float tim = 0;
  ImposedData(xphy, tim, wL);
  for(int iv = 0; iv < _M; iv++) {
    int imemL =  Varindex(ipgL, iv) + woffset;
    wn[imemL] = wL[iv];
    //wn[imemL] = ix;
  }
}

__kernel
void DGVolume(int ie,                    // 1: macrocel index
	      __constant float *physnodes, // 2: macrocell nodes
              __global float *wn,         // 3: field values
	      __global float *dtwn       // 4: time derivative
	      )
{

  __constant float *physnode = physnodes + ie * 3 * _NHEXA;

  int ix = get_global_id(0);
  int iy = get_global_id(1); 
  int iz = get_global_id(2);
 
  int woffset = ie * _M * _NPG_MACROCELL;

  // subcell id
  int icL[3];
  icL[0] = ix / _NP;
  icL[1] = iy / _NP;
  icL[2] = iz / _NP;

  
  int p[3];
  p[0] = ix % _NP;
  p[1] = iy % _NP;
  p[2] = iz % _NP;


  
  // ref coordinates
  float hx = 1. / (float)_RAF;
  float hy = 1. / (float)_RAF;
  float hz = 1. / (float)_RAF;

  int offset[3] = {gauss_lob_offset[_DEG] + p[0],
		   gauss_lob_offset[_DEG] + p[1],
		   gauss_lob_offset[_DEG] + p[2]};

  float x = hx * (icL[0] + gauss_lob_point[offset[0]]);
  float y = hy * (icL[1] + gauss_lob_point[offset[1]]);
  float z = hz * (icL[2] + gauss_lob_point[offset[2]]);

  float wpg = hx * hy * hz
    * gauss_lob_weight[offset[0]]
    * gauss_lob_weight[offset[1]]
    * gauss_lob_weight[offset[2]];

  float codtau[3][3];
  {
    float dtau[3][3];
    //printf("xyz=%f %f %f\n",x,y,z);
    get_dtau(x, y, z, physnode, dtau); // 1296 mults
    /* printf("dtau=\n%f %f %f \n%f %f %f \n%f %f %f\n", */
    /* 	   dtau[0][0],dtau[0][1],dtau[0][2], */
    /* 	   dtau[1][0],dtau[1][1],dtau[1][2], */
    /* 	   dtau[2][0],dtau[2][1],dtau[2][2]); */
  /* for(int ii=0;ii < 8;ii++){ */
  /*     printf("physnode=%f %f %f\n", */
  /* 	     physnode[3*ii+0],physnode[3*ii+1],physnode[3*ii+2]); */
  /*   } */
    
    codtau[0][0] =  dtau[1][1] * dtau[2][2] - dtau[1][2] * dtau[2][1];
    codtau[0][1] = -dtau[1][0] * dtau[2][2] + dtau[1][2] * dtau[2][0];
    codtau[0][2] =  dtau[1][0] * dtau[2][1] - dtau[1][1] * dtau[2][0];
    codtau[1][0] = -dtau[0][1] * dtau[2][2] + dtau[0][2] * dtau[2][1];
    codtau[1][1] =  dtau[0][0] * dtau[2][2] - dtau[0][2] * dtau[2][0];
    codtau[1][2] = -dtau[0][0] * dtau[2][1] + dtau[0][1] * dtau[2][0];
    codtau[2][0] =  dtau[0][1] * dtau[1][2] - dtau[0][2] * dtau[1][1];
    codtau[2][1] = -dtau[0][0] * dtau[1][2] + dtau[0][2] * dtau[1][0];
    codtau[2][2] =  dtau[0][0] * dtau[1][1] - dtau[0][1] * dtau[1][0];
  }

  float wL[_M];
  int ipgL = ix + _NPG_ROW * (iy + _NPG_ROW * iz);

  for(int iv = 0; iv < _M; iv++){
    int imemL =  Varindex(ipgL, iv) + woffset;
    wL[iv] = wn[imemL];
  }

  float flux[_M];
  int q[3] = {p[0], p[1], p[2]};

    // Loop on the "cross" points
    for(int iq = 0; iq < _NP; iq++) {
      q[0] = (p[0] + iq) % _NP;
      float dphiref[3] = {0, 0, 0};
      dphiref[0] = dlag(_DEG, q[0], p[0]) * _RAF;
      float dphi[3];
      for(int ii = 0; ii < 3; ii++) {
	float *codtauii = codtau[ii];
	dphi[ii]
	  = codtauii[0] * dphiref[0]
	  + codtauii[1] * dphiref[1]
	  + codtauii[2] * dphiref[2];
      }

      NumFlux(wL, wL, dphi, flux); // 3m mults when using NumFlux
 
      int ipgR = q[0] + icL[0] *  _NP + _NPG_ROW * (iy + _NPG_ROW * iz);
      for(int iv = 0; iv < _M; iv++) {
      int imemR =  Varindex(ipgR, iv) + woffset;
      //printf("imemR=%d wpg=%f\n",imemR,wpg);

     	dtwn[imemR] += flux[iv] * wpg;
      }
    }

    barrier(CLK_LOCAL_MEM_FENCE);
    
    for(int iv = 0; iv < _M; iv++){
      int imemL =  Varindex(ipgL, iv) + woffset;
      dtwn[imemL] /= wpg;
    }
    
    

}

// Compute the surface terms inside one macrocell
__kernel
void DGFlux(int ie,                      // 1: macrocell index
	    __constant float *physnodes, // 3: macrocell nodes
	    float tnow,
	    __global   float *wn,       // 4: field values
	    __global   float *dtwn     // 5: time derivative
	    )
{

  __constant float *physnode = physnodes + ie * 3 * _NHEXA;

  int ifx = get_global_id(0); // face id in x direction: 0.._RAF
  int iy = get_global_id(1);// pg id in other directions: 0.._NPG_ROW
  int iz = get_global_id(2);
 
  int woffset = ie * _M * _NPG_MACROCELL;

  // subcell id
  int icL[3];
  icL[0] = ifx - 1;
  icL[1] = iy / _NP;
  icL[2] = iz / _NP;

  int pL[3];
  pL[0] = _NP - 1;
  pL[1] = iy % _NP;
  pL[2] = iz % _NP;

  int icR[3];

  icR[0] = ifx;
  icR[1] = icL[1];
  icR[2] = icL[2];

  int pR[3];
  pR[0] = 0;
  pR[1] = iy % _NP;
  pR[2] = iz % _NP;
  
  float hx = 1.0 / _RAF;
  float hy = 1.0 / _RAF;
  float hz = 1.0 / _RAF;

  int offset[3];
  float x,y,z;
  //if (ifx > 0){
    offset[0]=gauss_lob_offset[_DEG] + pL[0];
    offset[1]=gauss_lob_offset[_DEG] + pL[1];
    offset[2]=gauss_lob_offset[_DEG] + pL[2];
    x = hx * (icL[0] + gauss_lob_point[offset[0]]);
    y = hy * (icL[1] + gauss_lob_point[offset[1]]);
    z = hz * (icL[2] + gauss_lob_point[offset[2]]);
  /* } else { */
  /*   //printf("ifx=%d pR=%d %d %d\n",ifx,pR[0],pR[1],pR[2]); */
  /*   offset[0]=gauss_lob_offset[_DEG] + pR[0]; */
  /*   offset[1]=gauss_lob_offset[_DEG] + pR[1]; */
  /*   offset[2]=gauss_lob_offset[_DEG] + pR[2]; */
  /*   x = hx * (icR[0] + gauss_lob_point[offset[0]]); */
  /*   y = hy * (icR[1] + gauss_lob_point[offset[1]]); */
  /*   z = hz * (icR[2] + gauss_lob_point[offset[2]]); */
  /* } */
   

  float codtau[3][3];
  {
    float dtau[3][3];
    get_dtau(x, y, z, physnode, dtau);
    codtau[0][0] =  dtau[1][1] * dtau[2][2] - dtau[1][2] * dtau[2][1];
    codtau[0][1] = -dtau[1][0] * dtau[2][2] + dtau[1][2] * dtau[2][0];
    codtau[0][2] =  dtau[1][0] * dtau[2][1] - dtau[1][1] * dtau[2][0];
    codtau[1][0] = -dtau[0][1] * dtau[2][2] + dtau[0][2] * dtau[2][1];
    codtau[1][1] =  dtau[0][0] * dtau[2][2] - dtau[0][2] * dtau[2][0];
    codtau[1][2] = -dtau[0][0] * dtau[2][1] + dtau[0][1] * dtau[2][0];
    codtau[2][0] =  dtau[0][1] * dtau[1][2] - dtau[0][2] * dtau[1][1];
    codtau[2][1] = -dtau[0][0] * dtau[1][2] + dtau[0][2] * dtau[1][0];
    codtau[2][2] =  dtau[0][0] * dtau[1][1] - dtau[0][1] * dtau[1][0];
  }

  float h1h2 = 1.0 / _RAF / _RAF;
  float vnds[3];
  vnds[0] = codtau[0][0] * h1h2;
  vnds[1] = codtau[1][0] * h1h2;
  vnds[2] = codtau[2][0] * h1h2;


  int ipgL = -1;
  if (ifx > 0)
    ipgL = pL[0] + _NP * icL[0] + _NPG_ROW * (iy + _NPG_ROW * iz);
  int ipgR = -1;
  if (ifx < _RAF)
    ipgR = pR[0] + _NP * icR[0] + _NPG_ROW * (iy + _NPG_ROW * iz);

  float xphy[3];
  get_tau(x, y, z, physnode, xphy);

  
  float wL[_M], wR[_M];
  float tim = tnow;
  for(int iv = 0; iv < _M; iv++) {
    if (ipgL >= 0){
      int imemL =  Varindex(ipgL, iv) + woffset;
      wL[iv] = wn[imemL];
    } else {
      ImposedData(xphy, tim, wL);
    }
    if (ipgR >= 0){
      int imemR =  Varindex(ipgR, iv) + woffset;
      wR[iv] = wn[imemR];
    } else {
      ImposedData(xphy, tim, wR);      
    }
  }

  float flux[_M];
  NumFlux(wL, wR, vnds, flux); //
  //printf("wL=%f %f\n",wL[0],wL[1]);
  float wpgs = wglop(_DEG, pL[1]) * wglop(_DEG, pL[2]);
  
  for(int iv = 0; iv < _M; ++iv) {
    if (ipgL >= 0){
      int imemL =  Varindex(ipgL, iv) + woffset;
      //printf("imemL=%d wpgs=%f\n",imemL,wpgs);
      dtwn[imemL] -= flux[iv] * wpgs;
    }
    
    
    if (ipgR >= 0){
      int imemR =  Varindex(ipgR, iv) + woffset;
      //printf("imemR=%d\n",imemR);
      dtwn[imemR] += flux[iv] * wpgs;
    }
  }
  
}


