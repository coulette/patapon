#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <omp.h>
#include <stdbool.h>
#include <float.h>

#define nx ((size_t) 1024)
#define ny ((size_t) 1024)
#define Lx (1.f)
#define Ly (1.f)

#define dx (Lx / (float)nx)
#define dy (Ly / (float)ny)

#define velx (1.f)
#define vely (1.f)
#define vmax (sqrtf(velx * velx + vely * vely))
static float Tmax;
#define cfl (0.8f)
static float dt;

static inline float fluxnum(float wL, float wR, float vnormx, float vnormy) {
  float vdotn = velx * vnormx + vely * vnormy;
  float vnp = vdotn > 0.f ? vdotn : 0;
  float vnm = vdotn - vnp;
  float flux = vnm * wR + vnp * wL;
  return flux;
}

static float exact_sol(float pos[2], float t) {
  float x = pos[0] - t * velx - 0.5f;
  float y = pos[1] - t * vely - 0.5f;
  float d2 = x * x + y * y;
  return expf(-30.f * d2);
}

#define relative_precision 1e-3f
static inline bool float_equal(float a, float b) {
  return a - b <= relative_precision && a - b >= -relative_precision;
}

static void verify(size_t nvalx, size_t nvaly, float wn[nvalx][nvaly], float t) {
  for (size_t i = 1; i < nx-1; ++i) {
    for (size_t j = 1; j < ny-1; ++j) {
      float xy[2] = {(float)i * dx + dx/2.f, (float)j * dy + dy/2.f};
      float exact_val = exact_sol(xy, t);
      if (!float_equal(exact_val, wn[i][j])) {
        fprintf(stderr, "Not goot at (%zu , %zu) values exact %f computed %f\n", i, j, (double) exact_val, (double)wn[i][j]);
        exit(1);
      }
    }
  }
}

static void* solve_c(void) {
  const float border_val = 0.f;
  float t = 0.f;
  float (*wn)[ny] = aligned_alloc(64, (nx*ny*sizeof(float)/64)*64 + 64);
  float (*wnp1)[ny] = aligned_alloc(64, (nx*ny*sizeof(float)/64)*64 + 64);
  for (size_t j = 0; j < ny; ++j) {
      wn[0][j] = border_val;
      wnp1[0][j] = border_val;
      wn[nx-1][j] = border_val;
      wnp1[nx-1][j] = border_val;
  }
  for (size_t i = 0; i < nx; ++i) {
      wn[i][0] = border_val;
      wnp1[0][0] = border_val;
      wn[i][ny-1] = border_val;
      wnp1[i][ny-1] = border_val;
  }
  for (size_t i = 1; i < nx-1; ++i) {
    for (size_t j = 1; j < ny-1; ++j) {
      float xy[2] = {(float)i * dx + dx/2.f, (float)j * dy + dy/2.f};
      wn[i][j] = exact_sol(xy, t);
      wnp1[i][j] = wn[i][j];
    }
  }

  float (*outwn)[ny] = NULL;
  size_t iter = 0;
  double elapsed = 0.;
  const float dt_dx = dt / dx;
  const float dt_dy = dt / dy;
  double start = omp_get_wtime();
  const size_t modulo_num_print = (size_t) ((Tmax-t)/dt * 0.1f);

#pragma omp parallel shared(dt, iter, elapsed, Tmax, t, outwn) firstprivate(wn, wnp1)
  {
    int percent_done = 10;
    float innert = t;
    while (innert < Tmax) {
      innert = innert + dt;
#pragma omp master
      {
        iter++;
        if (iter % modulo_num_print == 0) {
          printf("Done %d%%\n", percent_done);
          percent_done +=10;
        }
      }
#pragma omp for schedule(static)
      for (size_t i = 1; i < nx-1; ++i) {
        for (size_t j = 1; j < ny-1; ++j) {
          wnp1[i][j] = wn[i][j] - dt_dx * fluxnum(wn[i][j], wn[i+1][j], 1.f, 0.f);
          wnp1[i][j] -= dt_dx * fluxnum(wn[i][j], wn[i-1][j], -1.f, 0.f);
          wnp1[i][j] -= dt_dy * fluxnum(wn[i][j], wn[i][j+1], 0.f, 1.f);
          wnp1[i][j] -= dt_dy * fluxnum(wn[i][j], wn[i][j-1], 0.f, -1.f);
        }
      }

      float (*tmp)[ny] = wnp1;
      wnp1 = wn;
      wn = tmp;
    }
#pragma omp master
    {
      outwn = wn;
      t = innert;
    }
  }
  elapsed += omp_get_wtime() - start;
  printf("iter=%zu, t=%f, elapsed (s)=%f\n",iter, (double)t, elapsed);

  if (outwn == wnp1) {
      float (*tmp)[ny] = wnp1;
      wnp1 = wn;
      wn = tmp;
  }

  free (wnp1);
  /*verify(nx, ny, wn, t);*/
  return wn;
}

int main(void) {
  /* Denormalized floating point may hurt perf a lot ! (Try to comment the next 4 lines) */
#define CSR_FLUSH_TO_ZERO         (1 << 15)
  unsigned csr = __builtin_ia32_stmxcsr();
  csr |= CSR_FLUSH_TO_ZERO;
  __builtin_ia32_ldmxcsr(csr);

  Tmax = 0.5f / vmax;
  dt = cfl * (dx * dy) / (2.f * dx + 2.f * dy) / vmax;
  /*fprintf(stderr, "nx %zu\nny %zu\nLx %f\nLy %f\n", nx, ny, Lx, Ly);*/
  /*fprintf(stderr, "dx %f\ndy %f\nvmax %f\nTmax %f\ndt %f\n", dx, dy, vmax, Tmax, dt);*/
  float (*wn)[ny] = solve_c();
  fprintf(stderr, "%p\n", (void*) wn);
  free(wn);

  return EXIT_SUCCESS;
}
