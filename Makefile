.PHONY: all clean

DEBUG    := n
SANITIZE := n

ifeq ($(SANITIZE), y)
SANITIZEFLAGS := -fsanitize=address -fsanitize=undefined
endif

ifeq ($(DEBUG), y)
WARNFLAGS := -Wall -Wpedantic -Wextra -Waddress -Waggressive-loop-optimizations \
  -Wcast-qual -Wcast-align -Wmissing-declarations \
  -Wdouble-promotion -Wuninitialized -Winit-self \
  -Wstrict-aliasing -Wsuggest-attribute=const -Wtrampolines -Wfloat-equal \
  -Wshadow -Wunsafe-loop-optimizations -Wfloat-conversion -Wlogical-op \
  -Wnormalized -Wdisabled-optimization -Whsa -Wconversion -Wunused-result

CPPFLAGS += -DDEBUG
OPTIFLAGS := -Og
else
CPPFLAGS += -DNDEBUG
OPTIFLAGS := -O3 -march=native -flto -fno-math-errno
endif

CPPFLAGS += -I include
CFLAGS += -std=c11 -g $(WARNFLAGS) $(OPTIFLAGS) $(SANITIZEFLAGS) -fopenmp
LDFLAGS += -g $(OPTIFLAGS) $(SANITIZEFLAGS) -lm -fopenmp

all: transport_openmp

transport_openmp: transport_openmp.o
	$(CC) $^ $(LDFLAGS) -o $@

transport_openmp.o: transport_openmp.c

clean:
	rm transport_openmp.o transport_openmp
