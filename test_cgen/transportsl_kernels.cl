#define _NC _nc_
#define _NVS _nvs_
#define _NV _nv_
#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _INTERP_D _interp_d_
#define _NGRID (_NX*_NY)
#define _NSTENCIL (2 * _INTERP_D  +2)
#define real _real_
__constant int offset[_NVS][2] = _offset_;
__constant real locshift[_NVS][2] = _locshift_;
__constant real lag_coeffs_x[_NVS][_NSTENCIL] = _lag_coeffs_x_;
__constant real lag_coeffs_y[_NVS][_NSTENCIL] = _lag_coeffs_y_;

inline int get_memindex(int ix, int iy, int iv){

  int index = 0;
  index = iv * (_NGRID) + _NX * iy + ix;
  return index;

  }

inline int get_ivindex(int ivloc,int ivar){

  int index = 0;
  index = ivar * _NVS + ivloc;
  return index;

}


__kernel void init_sol(__global  real *fn){

  int id = get_global_id(0);
  //int size = get_global_size(0);
  
  int i = id % _NX;

  int j = id / _NX;
  
  real xy[2] = {i * _DX + _DX / 2 - 0.5_F, j * _DY + _DY / 2 - 0.5_F};

  for(int ik = 0; ik < _NV; ik++){
    int imem = get_memindex(i,j,ik);

    fn[imem] = exp (-20.0_F * (xy[0] * xy[0] + xy[1] * xy[1]));
  }
}


__kernel void transport_xO1(__global  real *fn, __global real *fnp1){

  int id = get_global_id(0);
  
  int i = id % _NX;
  
  int j = id / _NX;
  
  
  real fnow[_NV]; 
  for (int ivloc = 0; ivloc < _NVS; ivloc++){
    real fshift2 = locshift[ivloc][0];
    real fshift1 = 1._F-fshift2;
    int iorig1 = (i - offset[ivloc][0] + _NX) % _NX;
    int iorig2 = (iorig1 + 1 + _NX) % _NX;
    for (int ivar = 0; ivar < _NC; ivar++){
      int iv = get_ivindex(ivloc,ivar);
      int imem1 = get_memindex(iorig1,j,iv);
      int imem2 = get_memindex(iorig2,j,iv); 
      //int imem  = get_memindex(i,j,iv);
      fnow[iv] = fshift1 * fn[imem1] + fshift2 * fn[imem2];
      //fnow[iv] = fn[imem1];
    } 
  } // end iv
  
  for (int iv = 0; iv < _NV; iv++){
    int imem = get_memindex(i,j,iv);
    fnp1[imem] = fnow[iv];
  }
}

__kernel void transport_yO1(__global  real *fn, __global real *fnp1){

  int id = get_global_id(0);
  
  int i = id % _NX;
  
  int j = id / _NX;
  
  
  real fnow[_NV]; 
  for (int ivloc = 0; ivloc < _NVS; ivloc++){
    real fshift2 = locshift[ivloc][1];
    real fshift1 = 1._F-fshift2;
    int jorig1 = (j - offset[ivloc][1] + _NY) % _NY;
    int jorig2 = (jorig1 + 1 + _NY) % _NY;
    for (int ivar = 0; ivar < _NC; ivar++){
      int iv = get_ivindex(ivloc,ivar);
      int imem1 = get_memindex(i,jorig1,iv);
      int imem2 = get_memindex(i,jorig2,iv); 
      fnow[iv] = fshift1 * fn[imem1] + fshift2 * fn[imem2];
    } 
  } // end iv
  
  for (int iv = 0; iv < _NV; iv++){
    int imem = get_memindex(i,j,iv);
    fnp1[imem] = fnow[iv];
  }
}


__kernel void transport_xlag(__global  real *fn, __global real *fnp1){

  int id = get_global_id(0);
  
  int i = id % _NX;
  
  int j = id / _NX;
  
  
  real fnow[_NV]; 
  for (int ivloc = 0; ivloc < _NVS; ivloc++){
    int iorig[_NSTENCIL];
    for (int isten = 0; isten < _NSTENCIL; isten++){
      iorig[isten] = (i +isten -_INTERP_D -1 - offset[ivloc][0] + _NX) % _NX;
    }
    for (int ivar = 0; ivar < _NC; ivar++){
      int iv = get_ivindex(ivloc,ivar);
      fnow[iv] = 0._F;
      for (int isten = 0; isten <_NSTENCIL; isten++){
        int imem = get_memindex(iorig[isten],j,iv);
        fnow[iv] += fn[imem] * lag_coeffs_x[ivloc][isten];
      }
    } 
  } // end iv
  
  for (int iv = 0; iv < _NV; iv++){
    int imem = get_memindex(i,j,iv);
    fnp1[imem] = fnow[iv];
  }

}

__kernel void transport_ylag(__global  real *fn, __global real *fnp1){

  int id = get_global_id(0);
  
  int i = id % _NX;
  
  int j = id / _NX;
  
  
  real fnow[_NV]; 
  for (int ivloc = 0; ivloc < _NVS; ivloc++){
    int iorig[_NSTENCIL];
    for (int isten = 0; isten < _NSTENCIL; isten++){
      iorig[isten] = (j +isten -_INTERP_D -1 - offset[ivloc][1] + _NX) % _NX;
    }
    for (int ivar = 0; ivar < _NC; ivar++){
      int iv = get_ivindex(ivloc,ivar);
      fnow[iv] = 0._F;
      for (int isten = 0; isten <_NSTENCIL; isten++){
        int imem = get_memindex(i,iorig[isten],iv);
        fnow[iv] += fn[imem] * lag_coeffs_y[ivloc][isten];
      }
    } 
  } // end iv
  
  for (int iv = 0; iv < _NV; iv++){
    int imem = get_memindex(i,j,iv);
    fnp1[imem] = fnow[iv];
  }

}



