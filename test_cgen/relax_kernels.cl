#define _NC _nc_
#define _NVS _nvs_
#define _NV _nv_
#define _NX _nx_
#define _NY _ny_
#define _RELCOEFF _relcoeff_
#define _VMAX _vmax_
#define real _real_
#define _NGRID (_NX * _NY)

#define _COMPRELCOEFF (1._F - _RELCOEFF)

__constant real vi[_NV][2]= _vi_;

__constant real v0[2] = {1.0_F, 0.0_F};


inline int get_memindex(int ix, int iy, int iv){

  int index = 0;
  index = iv * (_NGRID) + _NX * iy + ix;
  return index;

  }

inline int get_ivindex(int ivloc,int ivar){

  int index = 0;
  index = ivar * _NVS + ivloc;
  return index;

}



void flux_phy( real w[_NC], real flux[_NC][2]){

  for (int k = 0; k < _NC; k++){
    flux[k][0] = v0[0] * w[k];
    flux[k][1] = v0[1] * w[k];
  }
}

void micro_to_macro( real f[_NV],real w[_NC]){

  for (int k  = 0; k < _NC; k++){
    w[k] = 0._F;
    for (int ivloc = 0; ivloc < _NVS; ivloc++){
      w[k] += f[ k * _NVS + ivloc];
    }
  }

}





void feq_D2Q4( real w[_NC], real f[_NV]){

  real flux[_NC][2];
  
  flux_phy(w,flux);
  
  for (int  k = 0; k < _NC ; k++){
    for (int ivloc  = 0; ivloc < _NVS ; ivloc++){
      int iv = get_ivindex(ivloc,k);
      f[iv] = 0.25_F * w[k] + (vi[ivloc][0] * flux[k][0] + vi[ivloc][1] * flux[k][1]) / (2.0 * _VMAX);
    }
  }

}



__kernel void relax_bgk(__global  real *fn, __global real *fnp1){

  int id = get_global_id(0);
  
  int i = id % _NX;
  
  int j = id / _NX;
  
  // get f
  real fnow[_NV];
  
  for (int iv = 0; iv < _NV; iv++){
    int imem = get_memindex(i,j,iv);
    fnow[iv] = fn[imem];
  }
  real wnow[_NC];
  
  real feq[_NV];
  
  feq_D2Q4(wnow,feq);
  
  for (int iv  = 0; iv < _NV ; iv++){
    fnow[iv] = _RELCOEFF * feq[iv] + _COMPRELCOEFF * fnow[iv]; 
  } 
  
  
  for (int iv = 0; iv < _NV; iv++){
    int imem = get_memindex(i,j,iv);
    fnp1[imem] = fnow[iv];
  }
  
}

