#define _NC _nc_
#define _NVS _nvs_
#define _NV _nv_
#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _VMAX _vmax_
#define _T _t_
#define real _real_
#define _NGRID (_NX * _NY)


__constant real vi[_NV][2]= _vi_;
__constant real v0[2] = {1.0_F, 0.0_F};

inline int get_memindex(int ix, int iy, int iv){

  int index = 0;
  index = iv * (_NGRID) + _NX * iy + ix;
  return index;

  }

inline int get_ivindex(int ivloc,int ivar){

  int index = 0;
  index = ivar * _NVS + ivloc;
  return index;

}



void flux_phy( real w[_NC], real flux[_NC][2]){

  for (int k = 0; k < _NC; k++){
    flux[k][0] = v0[0] * w[k];
    flux[k][1] = v0[1] * w[k];
  }

}





void feq_D2Q4( real w[_NC], real f[_NV]){

  real flux[_NC][2];
  
  flux_phy(w,flux);
  
  for (int  k = 0; k < _NC ; k++){
    for (int ivloc  = 0; ivloc < _NVS ; ivloc++){
      int iv = get_ivindex(ivloc,k);
      f[iv] = 0.25_F * w[k] + (vi[ivloc][0] * flux[k][0] + vi[ivloc][1] * flux[k][1]) / (2.0 * _VMAX);
    }
  }

}

void analytical_sol(real t,real xin[2],real w[_NC]){
  
  real x[2] = {xin[0] - 0.5_F - v0[0] * t, xin[1] - 0.5_F - v0[1] * t};
  
  for (int k = 0; k < _NC; k++){
  
    w[k] = exp (-100.0_F * (x[0] * x[0] + x[1] * x[1]));

  }
}

__kernel void exact_sol(__global  real *fn){

  int id = get_global_id(0);
  
  int i = id % _NX;
  
  int j = id / _NX;

  real x[2] = { i * _DX , j * _DY };
  
  real w[_NC];
  
  analytical_sol(_T,x,w);
  
  real fnow[_NV];
  
  feq_D2Q4(w,fnow);
  
  for (int iv = 0; iv < _NV; iv++){
    int imem = get_memindex(i,j,iv);
    fn[imem] = fnow[iv];
  }
  
}
