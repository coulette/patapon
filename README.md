# How to run `lbm_cl.py` within a remote Jupyter notebook

- Connect to the remote machine, say `gpu3`, without X redirection:

```bash
ssh gpu3.math.unistra.fr
```

- First time: set your jupyter configuration

```bash
jupyter notebook --generate-config
```

Add these lines to your `~.jupyter/jupyter_notebook_config.py` file:

```bash
c.NotebookApp.ip = 'gpu3.math.unistra.fr'
c.NotebookApp.open_browser = False
```

If you want to secure your notebook with a password, follow [these instructions](http://jupyter-notebook.readthedocs.io/en/stable/public_server.html#preparing-a-hashed-password).

- In `patapon` directory, run:

```bash
~/patapon$ jupyter-notebook 
[I 17:42:03.286 NotebookApp] Serving notebooks from local directory: /ssd/home/boileau/patapon
[I 17:42:03.287 NotebookApp] 0 active kernels
[I 17:42:03.287 NotebookApp] The Jupyter Notebook is running at:
[I 17:42:03.287 NotebookApp] http://gpu3.math.unistra.fr:8888/
[I 17:42:03.287 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[I 17:42:13.375 NotebookApp] 302 GET / (130.79.4.11) 1.15ms

```

- In your web browser, connect to the URL (here: <http://gpu3.math.unistra.fr:8888/>) and open `orszag-tang.ipynb`

- You may also run directly:

```
~/patapon$ jupyter-notebook orszag-tang.ipynb
```

