/*
 * Equations.cpp
 *
 *  Created on: Oct 4, 2017
 *      Author: lukas
 */

#include "Equations.h"

Equations::Equations(int NumConVar,double Lambda,double* VN1,double* VN2) {

	numberOfConsVar = NumConVar;
	normalVector1 = VN1;
	normalVector2 = VN2;
	lambda = Lambda;

}

double* Equations::Cons2EquilibriumFunc(Grid* grid,int cellIndexI,int cellIndexJ){

	double* equilibriumFunctions = new double[numberOfConsVar * grid->numberOfdirections];
	double* flux1 = FluxInDirection1(grid,cellIndexI,cellIndexJ);

    if(grid->numberOfdirections == 5){
    	double* flux2 = FluxInDirection2(grid,cellIndexI,cellIndexJ);
		for(int c = 0; c < numberOfConsVar; c++){
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionRight] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 5.0 - flux1[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionLeft] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 5.0 + flux1[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionUp] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 5.0 - flux2[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionDown] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 5.0 + flux2[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionDown + 1] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 5.0;
		}
		delete[] flux2;
		flux2 = 0;
    }else if(grid->numberOfdirections == 4){
    	double* flux2 = FluxInDirection2(grid,cellIndexI,cellIndexJ);
		for(int c = 0; c < numberOfConsVar; c++){
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionRight] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 4.0 - flux1[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionLeft] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 4.0 + flux1[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionUp] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 4.0 - flux2[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionDown] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 4.0 + flux2[c] / 2.0 / lambda;
		}
		delete[] flux2;
		flux2 = 0;
    }else if(grid->numberOfdirections == 3){
    	for(int c = 0; c < numberOfConsVar; c++){
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionRight] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 3.0 - flux1[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionLeft] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 3.0 + flux1[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionLeft + 1] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 3.0;
    	}
    }
    else if(grid->numberOfdirections == 2){
		for(int c = 0; c < numberOfConsVar; c++){
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionRight] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 2.0 - flux1[c] / 2.0 / lambda;
			equilibriumFunctions[c * grid->numberOfdirections + grid->indexDirectionLeft] = grid->conservativeVariables[cellIndexI][cellIndexJ][c] / 2.0 + flux1[c] / 2.0 / lambda;
		}
	}
    else{
    	printf("Number of directions is neither 2, 3 nor 4! -> check directions/dimensions \n");
    	exit(EXIT_FAILURE);
    }
	delete[] flux1;
	flux1 = 0;

	return equilibriumFunctions;
}

double* Equations::FluxInDirection1(Grid* grid,int cellIndexI,int cellIndexJ){return Flux(grid,normalVector1,cellIndexI,cellIndexJ);}
double* Equations::FluxInDirection2(Grid* grid,int cellIndexI,int cellIndexJ){return Flux(grid,normalVector2,cellIndexI,cellIndexJ);}


void Equations::computeInitialKineticData(Grid* grid){

	for(int i = 0; i < grid->Nx; i++){
			for(int j = 0; j < grid->Ny; j++){
				grid->kineticVariables[i][j] = Cons2EquilibriumFunc(grid,i,j);
			}
	}

}
