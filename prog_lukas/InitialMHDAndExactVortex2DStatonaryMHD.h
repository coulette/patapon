/*
 * InitialAndExactVortex2DStatonaryMHD.h
 *
 *  Created on: Aug 1, 2017
 *      Author: lukas
 */

#ifndef INITIALMHDANDEXACTVORTEX2DSTATONARYMHD_H_
#define INITIALMHDANDEXACTVORTEX2DSTATONARYMHD_H_

#include "InitialMHDAndExactVortex2D.h"

class InitialMHDAndExactVortex2DStatonaryMHD: public InitialMHDAndExactVortex2D {

	public:

		InitialMHDAndExactVortex2DStatonaryMHD(Grid* grid,EquationsMHD* equations,double Kappa,double Mu);
		double* getPositionWRTMovement(double* Position,double t);
		double getAdditionalMovementU(double u);
		double getAdditionalMovementV(double v);
		double getAdditionalMovementW(double w);
		double getDifferentsInMagneticStatusX(double Bx);
		double getDifferentsInMagneticStatusY(double By);
		double getDifferentsInMagneticStatusZ(double Bz);


};


#endif /* INITIALMHDANDEXACTVORTEX2DSTATONARYMHD_H_ */
