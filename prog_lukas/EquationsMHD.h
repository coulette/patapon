
#ifndef EQUATIONSMHD_H_
#define EQUATIONSMHD_H_

#include "Grid.h"
#include "Equations.h"
#include <iostream>
#include <cmath>

class EquationsMHD: public Equations {

	public:

		static const int indexDensity = 0;
		static const int indexMomentumX = 1;
		static const int indexMomentumY = 2;
		static const int indexMomentumZ = 3;
		static const int indexEnergy = 4;
		static const int indexMagneticFieldX = 5;
		static const int indexMagneticFieldY = 6;
		static const int indexMagneticFieldZ = 7;
		static const int indexDivergenceCleaningPotential = 8;

		double divergenceCleaningSpeed;
		double Gamma;

		EquationsMHD(int NumberOfConsVar, double Lambda, double DivergenceCleaningSpeed, double Gamma, double* NormalVector1, double* NormalVector2);

		double* Flux(Grid* Grid, double* NormalVector, int CellIndexI, int CellIndexJ);

		double computePressure(double* conservativeVariables);
		double computeSpeedOfSound(double* conservativeVariables);
		double computeMach(double* conservativeVariables);

		double* averageConservativeVariables(double* conservativeVariablesLeft, double* conservativeVariablesRight);

		double computeEulerEVi(double* conservativeVariables,int MomentumIndex,int i);
		double computeEulerEV1(double* conservativeVariables,int MomentumIndex);
		double computeEulerEV2(double* conservativeVariables,int MomentumIndex);
		double computeEulerEV3(double* conservativeVariables,int MomentumIndex);

		double computeCharacteristicVariableEuler1(double* conservativeVariables);
		double computeCharacteristicVariableEuler2(double* conservativeVariables);
		double computeCharacteristicVariableEuler3(double* conservativeVariables);
		double* computeCharacteristicVariablesEuler(double* conservativeVariables);

		double computeAbsoluteSpeedSquared(double* conservativeVariables);
		double computeAbsoluteMagneitcFieldSquared(double* conservativeVariables);
		double computeAbsoluteSpeed(double* conservativeVariables);
		double computeAbsoluteMagneticField(double* conservativeVariables);
		double computeAbsoluteMomentum(double* conservativeVariables);

};

#endif /* EQUATIONSMHD_H_ */
