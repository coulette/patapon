
//#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#define real _real_

#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _DT _dt_
#define _M _m_
#define _N _n_
#define _VX _vx_
#define _VY _vy_

#define _LAMBDA _lambda_

#define M_PI (3.14159265358979323846264338328_F)


#define _VOL (_DX * _DY)

__constant int dir[4][2] = { {-1, 0}, {1, 0},
			     {0, -1}, {0, 1}};

__constant real ds[4] = { _DY, _DY, _DX, _DX };



// physical flux of the hyperbolic system
/* void flux_phy(const real *w, const real* vnorm, real* flux){ */
/*   real vdotn = _VX * vnorm[0] + _VY * vnorm[1]; */
/*   int signe = 1; */
/*   for(int iv = 0; iv < _M; iv++){ */
/*     flux[iv] = vdotn * w[iv] * signe; */
/*     signe = -signe; */
/*   } */
/* } */

void flux_phy(const real *W, const real *vn, real *flux) {

  real gam = 1.6666666666_F;

  real un = (W[1] * vn[0] + W[3] * vn[1] + W[4] * vn[2]) / W[0];
  real bn = W[7] * vn[0] + W[5] * vn[1] + W[6] * vn[2];

  real p = (gam - 1) * (W[2]
		     - (W[1] * W[1] + W[3] * W[3]
			+ W[4] * W[4]) / 2 / W[0]
		    - (W[7] * W[7] + W[5] * W[5] + W[6] * W[6]) /2);

  flux[0] = W[0] * un;
  flux[1] = un*W[1] +
    (p + (W[7] * W[7] + W[5] * W[5] + W[6] * W[6]) / 2)
    * vn[0] - bn * W[7];
  flux[2] = (W[2] + p + (W[7]*W[7] + W[5]*W[5] + W[6]*W[6])/2)*un
    - (W[7]*W[1] + W[5]*W[3] + W[6]*W[4])*bn/W[0];
  flux[3] = un*W[3] + (p + (W[7]*W[7] + W[5]*W[5]
				      + W[6]*W[6])/2)*vn[1] - bn*W[5];
  flux[4] = un*W[4] + (p + (W[7]*W[7] + W[5]*W[5]
				      + W[6]*W[6])/2)*vn[2] - bn*W[6];

  flux[5] = -bn*W[3]/W[0] + un*W[5] + W[8]*vn[1];
  flux[6] = -bn*W[4]/W[0] + un*W[6] + W[8]*vn[2];
  flux[7] = -bn*W[1]/W[0] + un*W[7] + W[8]*vn[0];

  flux[8] = 6*6*bn;
}





// equilibrium "maxwellian" from macro data w
void w2f(const real *w, real *f){
  for(int d = 0; d < 4; d++){
    real flux[_M];
    real vnorm[2] = {(real) dir[d][0], (real) dir[d][1]};
    flux_phy(w, vnorm, flux);   
    for(int iv = 0; iv < _M; iv++){
      f[d * _M + iv] = w[iv] / 4 + flux[iv] / 2 / _LAMBDA;
    }
  }
}


// macro data w from micro data f
void f2w(const real *f, real *w){
  for(int iv = 0; iv < _M; iv++) w[iv] = 0;
  for(int d = 0; d < 4; d++){
    for(int iv = 0; iv < _M; iv++){
      w[iv] += f[d * _M + iv];
    }
  }
}


// exact macro data w
// transport
/* void exact_sol(real* xy, real t, real* w){ */
/*   int signe = 1; */
/*   for(int iv = 0; iv < _M; iv++){ */
/*     real x = xy[0] - t * _VX * signe - 0.5_F; */
/*     real y = xy[1] - t * _VY * signe - 0.5_F; */
/*     real d2 = x * x + y *y; */
/*     w[iv] = exp(-30*d2); */
/*     signe = -signe; */
/*   } */
/* } */


// mhd
void conservatives(real *y, real *w) {
  real gam = 1.6666666666_F;

  w[0] = y[0];   // rho
  w[1] = y[0]*y[1]; // rho u
  w[2] = y[2]/(gam-1) + y[0]*(y[1]*y[1]+y[3]*y[3]+y[4]*y[4])/2
    + (y[7]*y[7]+y[5]*y[5]+y[6]*y[6])/2; // rho E
  w[3] = y[0]*y[3];  // rho v
  w[4] = y[0]*y[4]; // rho w
  w[5] = y[5];   // By
  w[6] = y[6];   // Bz
  w[7] = y[7];        // Bx
  w[8] = y[8];        // psi
}


void exact_sol(real* x, real t, real* w){
  real gam = 1.6666666666_F;
  real yL[9];

  yL[0] = gam*gam;    //rho
  yL[1] = -sin(2 * M_PI * x[1]);   // u
  yL[2] = gam;       // p
  yL[3] = sin(2 * M_PI * x[0]);  // v
  yL[4] = 0;             // w
  yL[5] = sin(4 * M_PI * x[0]);  // By
  yL[6] = 0;                // Bz
  yL[7] = -sin(2 * M_PI * x[1]);       // Bx
  yL[8] = 0;            // psi

  conservatives(yL, w);

}


// initial condition on the macro data
__kernel void init_sol(__global  real *fn){

  int id = get_global_id(0);
  
  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  real wnow[_M];

  real t = 0;
  real xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};
  
  exact_sol(xy, t, wnow);
  
  real fnow[_N];
  w2f(wnow, fnow);

  //printf("x=%f, y=%f \n",xy[0],xy[1]);
  // load middle value
  for(int ik = 0; ik < _N; ik++){
    int imem = i + j * _NX + ik * ngrid;
    fn[imem] =  fnow[ik];
    //fn[imem] = j;
  }

}



// one time step of the LBM scheme
__kernel void time_step(__global  real *fn, __global real *fnp1){

  int id = get_global_id(0);
  
  int i = id % _NX;
  int j = id / _NX;


  int ngrid = _NX * _NY;

  real fnow[_N];
  
  // periodic shift in each direction
  for(int d = 0; d < 4; d++){
    int iR = (i - dir[d][0] + _NX) % _NX;
    int jR = (j - dir[d][1] + _NY) % _NY;
    
    for(int iv = 0; iv < _M; iv++){
      int ik = d * _M + iv;
      int imem = iR + jR * _NX + ik * ngrid;
      fnow[ik] = fn[imem];
    }
  }

  real wnow[_M];
  f2w(fnow, wnow);
  
  real fnext[_N];
  // first order relaxation
  w2f(wnow, fnext);

  // second order relaxation
  for(int ik = 0; ik < _N; ik++)
    fnext[ik] = 1.9_F * fnext[ik] - 0.9_F * fnow[ik];

  // save
  for(int ik = 0; ik < _N; ik++){
      int imem = i + j * _NX + ik * ngrid;
      fnp1[imem] = fnext[ik];
      //fnp1[imem] = fnow[ik];
  }


  

}
