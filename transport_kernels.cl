#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _DT _dt_
#define _M _m_

#define _VX _vx_
#define _VY _vy_

#define _WBORD  _wbord_




#define _VOL (_DX * _DY)

__constant int dir[4][2] = { {1, 0}, {-1, 0},
			     {0, 1}, {0, -1}};

__constant float ds[4] = { _DY, _DY, _DX, _DX };



void fluxnum(float *wL, float *wR, float* vnorm, float* flux){
  float vdotn = _VX * vnorm[0] + _VY * vnorm[1];
  float vnp = vdotn > 0 ? vdotn : 0;
  float vnm = vdotn - vnp;

  for(int iv = 0; iv < _M; iv++){
    flux[iv] = vnm * wR[iv] + vnp * wL[iv];
  }
}

void exact_sol(float* xy, float t, float* w){
  float x = xy[0] - t * _VX - 0.5f;
  float y = xy[1] - t * _VY - 0.5f;
  float d2 = x * x + y *y;
  for(int iv = 0; iv < _M; iv++)
    w[0] = exp(-30*d2);
}

__kernel void init_sol(__global  float *wn){

  int id = get_global_id(0);
  
  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  float wnow[_M];

  float t = 0;
  float xy[2] = {i * _DX + _DX / 2, j * _DY + _DY / 2};
  
  exact_sol(xy, t, wnow);
  //printf("x=%f, y=%f \n",xy[0],xy[1]);
  // load middle value
  for(int iv = 0; iv < _M; iv++){
    int imem = i + j * _NX + iv * ngrid;
    wn[imem] =  wnow[iv];
    // boundary values
    if (i == 0 || i == _NX - 1 || j == 0 || j == _NY - 1)
      wn[imem] = _WBORD;
  }

}




__kernel void time_step(__global  float *wn, __global float *wnp1){

  int id = get_global_id(0);
  
  int i = id % _NX;
  int j = id / _NX;


  int ngrid = _NX * _NY;
  
  float wnow[_M];
  float wnext[_M];

  // load middle value
  for(int iv = 0; iv < _M; iv++){
    int imem = i + j * _NX + iv * ngrid;
    wnow[iv] = wn[imem];
    wnext[iv] = wnow[iv];
   }
  
  // only compute internal cells
  if (i > 0 && i < _NX - 1 && j > 0 && j < _NY - 1){
    float flux[_M];

    // loop on all directions:
    // idir = 0 (east)
    // idir = 1 (west)
    // idir = 2 (north)
    // idir = 3 (south)
    for(int idir = 0; idir < 4; idir++){
      float vn[2];
      vn[0] = dir[idir][0];
      vn[1] = dir[idir][1];
      int iR = i + dir[idir][0];
      int jR = j + dir[idir][1];
      // load neighbour values
      float wR[_M];
      for(int iv = 0; iv < _M; iv++) wR[iv] = _WBORD;
      for(int iv = 0; iv < _M; iv++){
	int imem = iv * ngrid + iR + jR * _NX;
	wR[iv] = wn[imem];
      }
      fluxnum(wnow, wR, vn, flux);

      // time evolution
      for(int iv = 0; iv < _M; iv++){
	wnext[iv] -= _DT * ds[idir] / _VOL * flux[iv];
      }   
    }
  }   // end test for internal cells
  
  // copy wnext into global memory
  // (including boundary cells)

  for(int iv = 0; iv < _M; iv++){
    int imem = iv * ngrid + i + j * _NX;
    wnp1[imem] = wnext[iv];
  }
 

}
