#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function
import pyopencl as cl
import numpy as np

source = """
__kernel void K1(__global  float *x){
  int i = get_global_id(0);
  x[i] = i;
}

__kernel void K2(__global  float *y){
  int i = get_global_id(0);
  y[i] = i * i;
}

  
__kernel void K3(__global  float const *x, __global  float const *y, __global  float *z){
  int i = get_global_id(0);
  z[i] = x[i] + y[i];
  //z[i] = get_local_id(0);
}
"""

ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)
mf = cl.mem_flags

taille = 2**8

x_gpu = cl.Buffer(ctx, mf.WRITE_ONLY, size=(taille * np.dtype('float32').itemsize))
y_gpu = cl.Buffer(ctx, mf.WRITE_ONLY, size=(taille * np.dtype('float32').itemsize))

z_cpu = np.empty((taille, ), dtype = np.float32)
z_gpu = cl.Buffer(ctx, mf.WRITE_ONLY, z_cpu.nbytes)

verif_cpu = np.fromfunction(lambda i: i * i + i, (taille, ), dtype = np.float32)

prg = cl.Program(ctx, source).build()

event = prg.K1(queue, (taille, ), None, x_gpu)
prg.K2(queue, (taille, ), None, y_gpu).wait()
prg.K3(queue, (taille, ), None , x_gpu, y_gpu, z_gpu, wait_for = [event]).wait()

cl.enqueue_copy(queue, z_cpu, z_gpu)

print(z_cpu - verif_cpu)



